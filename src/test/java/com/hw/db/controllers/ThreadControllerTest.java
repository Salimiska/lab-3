package com.hw.db.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.exceptions.DataAccessExceptionImpl;
import com.hw.db.models.Thread;
import com.hw.db.models.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;

@RunWith(JUnit4.class)
public class ThreadControllerTest {
    private final ThreadController threadController = new ThreadController();
    private static List<Thread> threads;
    private static User mike, york;
    private static List<Post> posts;
    private static Vote vote;

    @Before
    public void setUp() throws IOException {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Thread thread = new Thread("Me", now, "some forum", "some thread msg", "some slug", "some title", 10);
        thread.setId(0);
        Thread anotherThread = new Thread("You", now, "other forum", "other thread msg", "other slug", "other title", 10);
        anotherThread.setId(1);
        threads = Arrays.asList(thread, anotherThread);


        mike = new User("mike young", "mike@example.com", "Mike Young", "some guy");
        york = new User("york old", "york@example.com", "York Old", "other guy");

        Post post1 = new Post("Mike Young", now, null, "some msg 1", null, null, false);
        post1.setId(0);
        Post post2 = new Post("York OLD", now, null, "some msg 2", null, null, false);
        post2.setId(1);
        posts = new ArrayList<>(Arrays.asList(post1, post2));

        ObjectMapper objectMapper = new ObjectMapper();
        vote = objectMapper.readValue("{\"nickname\": \"Mike Young\", \"voice\": 5}", Vote.class);
    }

    @Test
    public void checkIdTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(threads.get(0));
            Thread res = threadController.checkIdOrSlug("0");
            assertThat(res).isEqualTo(threads.get(0));
            threadDAO.verify(() -> ThreadDAO.getThreadById(0));
        }
    }

    @Test
    public void checkSlugTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));
            Thread res = threadController.checkIdOrSlug("some slug");
            assertThat(res).isEqualTo(threads.get(0));
            threadDAO.verify(() -> ThreadDAO.getThreadBySlug("some slug"));
        }
    }

    @Test
    public void createPostTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                userDAO.when(() -> UserDAO.Info("Mike Young")).thenReturn(mike);
                userDAO.when(() -> UserDAO.Info("York OLD")).thenReturn(york);

                Function<Post, Boolean> isAuthorCorrect = (post) -> {
                    switch (post.getMessage()) {
                        case "some msg 1":
                            return post.getAuthor().equals(mike.getNickname());
                        case "some msg 2":
                            return post.getAuthor().equals(york.getNickname());
                        default:
                            return false;
                    }
                };
                Function<Post, Boolean> isCorrect = (post) -> {
                    if (post == null || post.getAuthor() == null || post.getThread() == null || post.getForum() == null)
                        return false;
                    return isAuthorCorrect.apply(post) && post.getThread().equals(threads.get(0).getId())
                            && post.getForum().equals(threads.get(0).getForum());
                };

                ResponseEntity res = threadController.createPost("some slug", posts);
                assertThat(posts.stream().allMatch(isCorrect::apply)).isTrue();
                assertThat(res).isEqualToComparingFieldByField(ResponseEntity.status(HttpStatus.CREATED).body(posts));

                userDAO.verify(() -> UserDAO.Info("Mike Young"));
                userDAO.verify(() -> UserDAO.Info("York OLD"));
                threadDAO.verify(() -> ThreadDAO.createPosts(threads.get(0), posts, Arrays.asList(mike, york)));
            }
        }
    }

    @Test
    public void createPostThreadNotFoundTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenThrow(new DataAccessExceptionImpl("some msg"));
            ResponseEntity res = threadController.createPost("some slug", posts);
            ResponseEntity<Message> expectedRes = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден."));
            assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
        }
    }

    @Test
    public void createPostIncorrectPostsTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));
            threadDAO
                    .when(() -> ThreadDAO.createPosts(threads.get(0), posts, Arrays.asList(mike, york)))
                    .thenThrow(new DataAccessExceptionImpl("some msg"));
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                userDAO.when(() -> UserDAO.Info("Mike Young")).thenReturn(mike);
                userDAO.when(() -> UserDAO.Info("York OLD")).thenReturn(york);
                ResponseEntity res = threadController.createPost("some slug", posts);
                ResponseEntity<Message> expectedRes = ResponseEntity.status(HttpStatus.CONFLICT).body(
                        new Message("Хотя бы один родительский пост отсутсвует в текущей ветке обсуждения.")
                );
                assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
                threadDAO.verify(() -> ThreadDAO.createPosts(threads.get(0), posts, Arrays.asList(mike, york)));
            }
        }
    }

    @Test
    public void getPostsTests() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));
            threadDAO.when(() -> ThreadDAO.getPosts(eq(0), eq(1), eq(0), anyString(), eq(false)))
                    .thenReturn(posts);

            ResponseEntity res = threadController.Posts("some slug", 1, 0, "flat", false);
            assertThat(res).isEqualToComparingFieldByField(ResponseEntity.status(HttpStatus.OK).body(posts));
            threadDAO.verify(() -> ThreadDAO.getPosts(0, 1, 0, "flat", false));
        }
    }

    @Test
    public void getPostsThreadNotFound() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenThrow(new DataAccessExceptionImpl("some msg"));

            ResponseEntity res = threadController.Posts("some slug", 1, 0, "flat", false);
            ResponseEntity<Message> expectedRes = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден."));
            assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
        }
    }

    @Test
    public void changeTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(threads.get(0));
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));
            threadDAO.when(() -> ThreadDAO.change(threads.get(0), threads.get(1))).thenAnswer(
                    invocationOnMock -> {
                        threads.get(0).setCreated(threads.get(1).getCreated());
                        threads.get(0).setSlug(threads.get(1).getSlug());
                        threads.get(0).setAuthor(threads.get(1).getAuthor());
                        threads.get(0).setMessage(threads.get(1).getMessage());
                        threads.get(0).setTitle(threads.get(1).getTitle());
                        threads.get(0).setForum(threads.get(1).getForum());
                        return null;
                    }
            );

            Function<Thread, Boolean> isChangedCorrectly = (th) -> th.getForum().equals(threads.get(1).getForum()) &&
                    th.getAuthor().equals(threads.get(1).getAuthor()) &&
                    th.getCreated().equals(threads.get(1).getCreated()) &&
                    th.getSlug().equals(threads.get(1).getSlug()) &&
                    th.getMessage().equals(threads.get(1).getMessage()) &&
                    th.getTitle().equals(threads.get(1).getTitle());
            ResponseEntity res = threadController.change("some slug", threads.get(1));
            ResponseEntity expectedRes = ResponseEntity.status(HttpStatus.OK).body(threads.get(0));
            assertThat(isChangedCorrectly.apply(threads.get(0))).isTrue();
            assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);

            threadDAO.verify(() -> ThreadDAO.getThreadById(0));
            threadDAO.verify(() -> ThreadDAO.getThreadBySlug("some slug"));
            threadDAO.verify(() -> ThreadDAO.change(threads.get(0), threads.get(1)));
        }
    }

    @Test
    public void changeThreadNotFound() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenThrow(new DataAccessExceptionImpl("some msg"));

            ResponseEntity res = threadController.change("some slug", threads.get(1));
            ResponseEntity<Message> expectedRes = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден."));
            assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
        }
    }

    @Test
    public void infoTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));

            ResponseEntity res = threadController.info("some slug");
            assertThat(res).isEqualToComparingFieldByField(ResponseEntity.status(HttpStatus.OK).body(threads.get(0)));
        }
    }

    @Test
    public void infoThreadNotFoundTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenThrow(new DataAccessExceptionImpl("some msg"));

            ResponseEntity res = threadController.info("some slug");
            ResponseEntity<Message> expectedRes = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден."));
            assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
        }
    }

    @Test
    public void createVoteTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));

            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                userDAO.when(() -> UserDAO.Info("Mike Young")).thenReturn(mike);

                Integer votesBefore = threads.get(0).getVotes();
                ResponseEntity res = threadController.createVote("some slug", vote);
                ResponseEntity expectedRes = ResponseEntity.status(HttpStatus.OK).body(threads.get(0));
                assertThat(vote.getTid().equals(threads.get(0).getId()) && vote.getNickname().equals(mike.getNickname()))
                        .isTrue();
                assertThat(threads.get(0).getVotes().equals(votesBefore + vote.getVoice()));
                assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);

                threadDAO.verify(() -> ThreadDAO.createVote(any(), eq(vote)));
                userDAO.verify(() -> UserDAO.Info("Mike Young"));
            }
        }
    }

    @Test
    public void createVoteThreadNotFoundTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenThrow(new DataAccessExceptionImpl("some msg"));

            ResponseEntity res = threadController.createVote("some slug", vote);
            ResponseEntity<Message> expectedRes = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Раздел не найден."));
            assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
        }
    }

    @Test
    public void createVoteDuplicateVoteTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));
            threadDAO.when(() -> ThreadDAO.createVote(any(), any())).thenThrow(new DuplicateKeyException("some msg"));
            threadDAO.when(() -> ThreadDAO.change(any(), anyInt())).thenThrow(new DuplicateKeyException("some msg"));

            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                userDAO.when(() -> UserDAO.Info("Mike Young")).thenReturn(mike);

                ResponseEntity res = threadController.createVote("some slug", vote);
                ResponseEntity expectedRes = ResponseEntity.status(HttpStatus.CONFLICT).body(new Message("Хотя бы один родительский пост отсутсвует в текущей ветке обсуждения."));

                assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);
                threadDAO.verify(() -> ThreadDAO.createVote(any(), eq(vote)));
                userDAO.verify(() -> UserDAO.Info("Mike Young"));
            }
        }
    }

    @Test
    public void createVoteDuplicateVoteCantChangeTest() {
        try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAO.when(() -> ThreadDAO.getThreadBySlug("some slug")).thenReturn(threads.get(0));
            threadDAO.when(() -> ThreadDAO.createVote(any(), any())).thenThrow(new DuplicateKeyException("some msg"));
            threadDAO.when(() -> ThreadDAO.change(any(), anyInt())).thenReturn(threads.get(0).getVotes() + vote.getVoice());

            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                userDAO.when(() -> UserDAO.Info("Mike Young")).thenReturn(mike);

                Integer votesBefore = threads.get(0).getVotes();
                ResponseEntity res = threadController.createVote("some slug", vote);
                ResponseEntity expectedRes = ResponseEntity.status(HttpStatus.OK).body(threads.get(0));
                assertThat(vote.getTid().equals(threads.get(0).getId()) && vote.getNickname().equals(mike.getNickname()))
                        .isTrue();
                assertThat(threads.get(0).getVotes().equals(votesBefore + vote.getVoice()));
                assertThat(res).isEqualToComparingFieldByFieldRecursively(expectedRes);

                threadDAO.verify(() -> ThreadDAO.createVote(any(), eq(vote)));
                threadDAO.verify(() -> ThreadDAO.change(vote, votesBefore));
                userDAO.verify(() -> UserDAO.Info("Mike Young"));
            }
        }
    }
}
